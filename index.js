#!/usr/bin/env node

const inquirer = require('inquirer');
const fetch = require('node-fetch');
const get = require('lodash/get');
const { spawn } = require('child_process');
const mpvAPI = require('node-mpv');
const fs = require('fs');
const os = require('os');

inquirer.registerPrompt('search-list', require('inquirer-search-list'));
const API_HOST = 'http://radio.garden/api';
const EXIT_MENU = null;
const MY_RADIOS_FILE = 'myRadios.json';
const MY_RADIOS_PATH = `${os.homedir}/.config/tradio`;

if (os.platform() === 'win32') {
  console.error(`Platform ${os.platform} no home user folder implemente. Pleas feel free to folk this proyect a implement win32 support ;)`);
  process.exit(-1);
}

function getRadiosList(radios) {
  return get(radios, 'hits.hits', [])
    .map(radio => get(radio, '_source', { code: '', placeId: '', title: '', type: '', url: '' }));
}

async function radioSelector({ radios }) {
  const answers = await inquirer.prompt([{
    type: 'search-list',
    message: `${radios.length} Stations, Pick one`,
    name: 'selected',
    choices: radios
      .filter(radio => radio.type === 'channel')
      .map(radio => ({
        name: `${radio.title} (${radio.subtitle} [${radio.code}])`,
        value: radio,
      })),
  }]);
  return {
    func: radioMenu,
    args: { selected: answers.selected, radios }
  };
}

function getMyRadioRecord() {
  let file = '[]';
  const path = `${MY_RADIOS_PATH}/${MY_RADIOS_FILE}`;
  if (fs.existsSync(path)) {
    file = fs.readFileSync(path, 'utf8');
  }
  return file;
}

async function myRadioSelector() {
  const myRadios = JSON.parse(getMyRadioRecord()); 
  if (!myRadios.length) return { func: mainMenu, args: {} };
  const answers = await inquirer.prompt([{
    type: 'search-list',
    message: `${myRadios.length} Stations, Pick one`,
    name: 'selected',
    choices: myRadios
      .filter(radio => radio.type === 'channel')
      .map(radio => ({
        name: `${radio.title} (${radio.subtitle} [${radio.code}])`,
        value: radio,
      })),
  }]);
  return {
    func: radioMenu,
    args: { selected: answers.selected, radios: myRadios }
  };
}

async function play({ selected, radios }) {
  try {
    const mpv = new mpvAPI();
    await mpv.start();
    await mpv.load(`${API_HOST}/ara/content/listen/${selected.channelId}/channel.mp3`);
    return { func: playing, args: { mpv, selected, radios } };
  } catch (error) {
    console.error(error);
  }
}

async function playing({ selected, radios, mpv }) {
  try {
    const answers = await inquirer.prompt([{
      type: '',
      name: 'selected',
      message: `Playing ${selected.title} (${selected.subtitle} [${selected.code}]). Stop it ?`,
      default: false,
    }]);
    mpv.stop();
    return { func: radioMenu, args: { selected, radios } };
  } catch (error) {
    console.error(error);
  }
}

async function addToFavorites({ selected, radios }) {
  try {
    const path = `${MY_RADIOS_PATH}/${MY_RADIOS_FILE}`;
    const myRadios = JSON.parse(getMyRadioRecord());
    if (!myRadios.some(radio => radio.channelId === selected.channelId)) {
      myRadios.push(selected);
      if (!fs.existsSync(MY_RADIOS_PATH)) fs.mkdirSync(MY_RADIOS_PATH, { recursive: true });
      fs.writeFileSync(path, JSON.stringify(myRadios), 'utf8');
    }
    return { func: radioMenu, args: { selected, radios} };
  } catch (error) {
    console.log(error);
  }
}

async function radioMenu({ selected, radios }) {
  try {
    const answers = await inquirer.prompt([{
      type: 'list',
      name: 'selected',
      message: `${selected.title} (${selected.subtitle} [${selected.code}])`,
      choices: [
        { name: 'Play', value: { func: play, args: { selected, radios } } }, 
        { name: 'Add to Favorites', value: { func: addToFavorites, args: { selected, radios } } },
        { name: 'Back radios selector', value: { func: radioSelector, args: { selected, radios } } },
        { name: 'Search other radio', value: { func: reqSearch, arg: {} } },
        { name: 'Exit', value: { func: mainMenu, args: {} } },
    ]}]);
    return get(answers, 'selected', { func: EXIT_MENU });
  } catch (error) {
    console.error(error);
  }
}

async function reqSearch() {
  try {
    const answers = await inquirer.prompt([{
      type: 'input',
      name: 'search',
      message: 'Radio to search',
    }]);
    const resp = await fetch(`${API_HOST}/search?q=${answers.search}`);
    const radios = await resp.json();
    return {
      args: {
        radios: getRadiosList(radios),
      },
      func: radioSelector,
    }
  } catch (error) {
    console.error(error);
  }
}

function myRadios() {
  console.log('My Radios');
}

async function mainMenu() {
  try {
    const answers = await inquirer.prompt([{
      type: 'list',
      name: 'menu',
      message: 'Menu',
      choices: [
        { name: 'My radios', value: { func: myRadioSelector, args: {} } }, 
        { name: 'Search', value: { func: reqSearch, args: {} } },
        { name: 'Exit', value: { func: EXIT_MENU, args: {} } },
    ]}]);
    return get(answers, 'menu', { func: EXIT_MENU });
  } catch (error) {
    console.error(error);
  }
}

async function flow(initAction = { func: EXIT_MENU }) {
  let action = initAction;
  while (true) {
    if (!action.func) break;
    action = await action.func(action.args);
  }
}

async function main() {
  flow({ func: mainMenu, args: {} });
}

main();
